<?php

namespace App\Model;

class ContactModel extends \Nette\Object {

    public function validate(array $data) {
        $errors = [];

        if(isset($data['fullname']) && strlen($data['fullname'])) {
            if(count(explode(' ', $data['fullname'])) != 2) {
                $errors[] = 'Vyplňte prosím jméno i příjmení';
            }
        }
        else {
            $errors[] = 'Vyplňte prosím jméno';
        }

        if(isset($data['phone-number']) && strlen($data['phone-number'])) {
            $error = false;

            $number = str_replace(' ', '', $data['phone-number']);

            $count = 0;
            $number = str_replace('+', '', $number, $count);

            if($count > 1 || ($count == 1 && strlen($number) != 12)) $error = true;

            if(!$error) $error = !preg_match('/^(\d{3})?\d{9}$/', $number);

            if($error) $errors[] = 'Telefonní číslo není ve správném formátu';
        }
        else $errors[] = 'Vyplňte prosím telefonní číslo.';

        if(isset($data['call-duration']) && strlen($data['call-duration'])) {
            if(!is_numeric($data['call-duration']) || $data['call-duration'] <= 0) $errors[] = 'Délka hovoru musí být kladné číslo';
        }
        else $errors[] = 'Vyplňte prosím délku hovoru';

        return $errors;

    }

    public function add($data) {
        $fileName = str_replace(' ', '-', $data['fullname']);
        $filePath = "../app/contacts/$fileName.txt";
        if(file_exists($filePath)) {
            $iterator = 2;
            while (file_exists("../app/contacts/$fileName-$iterator.txt")) $iterator++;
            $filePath = "../app/contacts/$fileName-$iterator.txt";
        }

        $data['phone-number'] = str_replace(' ', '', $data['phone-number']);
        $data['phone-number'] = str_replace('+', '', $data['phone-number']);

        if(strlen($data['phone-number']) == 9) {
            $data['phone-number'] = '420' . $data['phone-number'];
        }

        $phonenumber = '+' . substr($data['phone-number'], 0, 3) . " "
            . substr($data['phone-number'], 3, 3) . " "
            . substr($data['phone-number'], 6, 3) . " "
            . substr($data['phone-number'], 9, 3);


        $content = $data['fullname'] . "\t";
        $content .= $phonenumber. "\t";
        $content .= $data['call-duration'] . "\t";
        $content .= (isset($data['agree']) && $data['agree'] == 'on' ? 'Ano' : 'Ne');

        file_put_contents($filePath, $content);
    }

}