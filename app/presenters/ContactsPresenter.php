<?php

namespace App\Presenters;


class ContactsPresenter extends \Nette\Application\UI\Presenter {

    protected $contactModel;

    /**
     * ContactsPresenter constructor.
     * @param $contactModel \App\Model\ContactModel
     */
    public function __construct(\App\Model\ContactModel $contactModel) {
        $this->contactModel = $contactModel;
    }


    public function actionAddContact() {
        $data = $this->getRequest()->getPost();

        if(!count($errors = $this->contactModel->validate($data))) {
            $this->contactModel->add($data);
        }
        else {
            foreach ($errors as $error) {
                $this->flashMessage($error);
            }
        }
        \Tracy\Debugger::barDump($data);

        \Tracy\Debugger::barDump($this->contactModel->validate($data));

        $this->redirect('default');
    }
}